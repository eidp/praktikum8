#ifndef _WARUM_IST_CPP_SO_DUMM_BUCH
#define _WARUM_IST_CPP_SO_DUMM_BUCH

#include <string>

using namespace std;

class Buch {
    string autor, titel;
    unsigned int inventarnummer, benutzernummer;

    public:
        Buch(string autor, string titel, unsigned int inventarnummer) : autor(autor), titel(titel), inventarnummer(inventarnummer), benutzernummer(0) {}
        bool istVerliehen();
        unsigned int inventarNummer();
        unsigned int ausgeliehenVon();
        bool verleiheAn(unsigned int num);
        void rueckgabe();
        void print();
};

#endif