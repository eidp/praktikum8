#include <iostream>
#include <string>
#include "buch.h"

using namespace std;

void Buch::print() {
    cout << "Buch - Inventarnummer " << inventarnummer << ": " << titel << ", ";

    if (benutzernummer != 0)
        cout << "ausgeliehen von von Benutzer " << benutzernummer;
    else
        cout << "verfügbar";
    cout << endl;
}

bool Buch::istVerliehen() {
    return benutzernummer != 0;
}

unsigned int Buch::inventarNummer() {
    return inventarnummer;
}

unsigned int Buch::ausgeliehenVon() {
    return benutzernummer;
}

bool Buch::verleiheAn(unsigned int num) {
    if (benutzernummer == 0) {
        benutzernummer = num;
        return true;
    }

    return false;
}

void Buch::rueckgabe() {
    benutzernummer = 0;
}